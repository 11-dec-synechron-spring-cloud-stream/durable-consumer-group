package com.classpath.durableconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DurableConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DurableConsumerApplication.class, args);
    }

}
